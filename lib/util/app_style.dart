import 'package:flutter/material.dart';

class AppStyle {
  static TextStyle appBarText = const TextStyle(
    color: Colors.black,
    fontSize: 25,
  );

  static Icon backIcon = const Icon(
    Icons.arrow_back_ios,
    color: Colors.black,
    size: 30,
  );
  static Icon mapIcon = const Icon(
    Icons.navigation_outlined,
    color: Colors.black,
    size: 30,
  );
}
