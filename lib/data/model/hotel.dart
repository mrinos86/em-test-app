class Hotel {
  int id;
  String title;
  String description;
  String address;
  String postcode;
  String phoneNumber;
  String latitude;
  String longitude;
  String imageS;
  String imageM;
  String imageL;

  Hotel({
    required this.id,
    required this.title,
    required this.description,
    required this.address,
    required this.postcode,
    required this.phoneNumber,
    required this.latitude,
    required this.longitude,
    required this.imageS,
    required this.imageM,
    required this.imageL,
  });

  static Hotel fromMap(Map<String, dynamic> map) {
    return Hotel(
      id: map['id'],
      title: map['title'],
      description: map['description'],
      address: map['address'],
      postcode: map['postcode'],
      phoneNumber: map['phoneNumber'],
      latitude: map['latitude'],
      longitude: map['longitude'],
      imageS: map['image']['small'],
      imageM: map['image']['medium'],
      imageL: map['image']['large'],
    );
  }
}
