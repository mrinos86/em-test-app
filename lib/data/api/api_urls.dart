class APIURLs {
  //base url
  static const String baseURL = "https://dl.dropboxusercontent.com";

  // all api endpoints
  static const String getHotelData = "$baseURL/s/6nt7fkdt7ck0lue/hotels.json";
}
