class RequestData {
  String url;
  var header;
  var body;

  RequestData({
    required this.url,
    this.header,
    this.body,
  });
}
