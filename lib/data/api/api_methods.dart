import 'package:dio/dio.dart';
import 'package:em_test_app/data/api/request_data.dart';

class ApiMethod {
  Dio dio = Dio();
  late Response response;
  late Options options;

  Future<Response> getMethod(RequestData requestData) async {
    try {
      options = Options(headers: {
        'Content-Type': 'application/json',
      });
      response = await dio.get(requestData.url, options: options);
    } on DioError catch (e) {
      response = e.response!;
    }
    return response;
  }

  // here
  // write all other methods put,post
}
