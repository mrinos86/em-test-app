import 'package:em_test_app/data/model/user.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class AuthService {
  Future<User?> login() async {
    User? _userData;
    try {
      // by default we request the email and the public profile
      final LoginResult result = await FacebookAuth.instance.login();

      if (result.status == LoginStatus.success) {
        // get the user data
        final userData = await FacebookAuth.instance.getUserData();
        _userData = User.fromMap(userData);
      }
      return _userData;
    } catch (_) {
      return _userData;
    }
  }

  Future<User?> checkIfIsLogged() async {
    User? _userData;
    try {
      final accessToken = await FacebookAuth.instance.accessToken;

      if (accessToken != null) {
        final userData = await FacebookAuth.instance.getUserData();
        _userData = User.fromMap(userData);
      }
      return _userData;
    } catch (_) {
      return _userData;
    }
  }

  Future<bool> logOut() async {
    await FacebookAuth.instance.logOut();
    return true;
  }
}
