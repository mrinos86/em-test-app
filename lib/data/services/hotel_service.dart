import 'dart:convert';

import 'package:em_test_app/data/api/api_methods.dart';
import 'package:em_test_app/data/api/api_urls.dart';
import 'package:em_test_app/data/api/request_data.dart';
import 'package:em_test_app/data/model/hotel.dart';

class HotelService {
  ApiMethod api = ApiMethod();

  Future<List<Hotel>> getAllHotelData() async {
    String url = APIURLs.getHotelData;

    List<Hotel> _hotelList = [];

    try {
      var response = await api.getMethod(RequestData(url: url));
      dynamic jsonObject = (jsonDecode(response.data));
      var resData = jsonObject["data"];

      _hotelList = resData.map<Hotel>((snapshot) {
        final _list = Hotel.fromMap(snapshot);
        return _list;
      }).toList();
    } catch (_) {
      _hotelList = [];
    }
    return _hotelList;
  }
}
