import 'dart:async';

import 'package:em_test_app/data/model/hotel.dart';
import 'package:em_test_app/util/app_colors.dart';
import 'package:em_test_app/util/app_style.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  Hotel hotelData;
  MapScreen({Key? key, required this.hotelData}) : super(key: key);

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final Completer<GoogleMapController> _controller = Completer();
  final List<Marker> _markers = <Marker>[];

  late CameraPosition _place;

  late double _lat;
  late double _log;

  @override
  void initState() {
    super.initState();
    setState(() {
      _lat = double.parse(widget.hotelData.latitude);
      _log = double.parse(widget.hotelData.longitude);

      _place = CameraPosition(target: LatLng(_lat, _log), zoom: 14.4746);
    });
    _markers.add(Marker(
        markerId: const MarkerId('locId'),
        position: LatLng(_lat, _log),
        infoWindow: const InfoWindow(title: 'location')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary_color,
        leading: IconButton(
          padding: EdgeInsets.zero,
          icon: AppStyle.backIcon,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Map', style: AppStyle.appBarText),
        centerTitle: true,
      ),
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _place,
        markers: Set<Marker>.of(_markers),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }
}
