import 'package:em_test_app/data/model/hotel.dart';
import 'package:em_test_app/layouts/screens/map_screen.dart';
import 'package:em_test_app/util/app_colors.dart';
import 'package:em_test_app/util/app_style.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  Hotel hotelData;
  DetailScreen({Key? key, required this.hotelData}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  void _onPressedMap(Hotel data) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MapScreen(hotelData: data)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary_color,
        leading: IconButton(
          padding: EdgeInsets.zero,
          icon: AppStyle.backIcon,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Detail', style: AppStyle.appBarText),
        actions: <Widget>[
          IconButton(
            icon: AppStyle.mapIcon,
            onPressed: () {
              _onPressedMap(widget.hotelData);
            },
          )
        ],
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(23.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.network(
                  // widget.hotelData.imageM,
                  //api image url not working
                  "https://i.natgeofe.com/n/46b07b5e-1264-42e1-ae4b-8a021226e2d0/domestic-cat_thumb_square.jpg",
                  fit: BoxFit.cover,
                  height: 250,
                  width: MediaQuery.of(context).size.width / 2),
              const SizedBox(height: 20),
              Text(widget.hotelData.title,
                  style: const TextStyle(
                      fontSize: 22, fontWeight: FontWeight.w700)),
              const SizedBox(height: 20),
              Text(widget.hotelData.description,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey)),
            ],
          ),
        ),
      ),
    );
  }
}
