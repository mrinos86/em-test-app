import 'dart:async';

import 'package:em_test_app/bloc/auth_bloc/auth_bloc.dart';
import 'package:em_test_app/bloc/hotel_bloc/hotel_bloc.dart';
import 'package:em_test_app/layouts/screens/home_screen.dart';
import 'package:em_test_app/layouts/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 1), () => _authInit());
  }

  _authInit() {
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _authBloc.add(AuthInitEvent());
  }

  void _authBlocListner(BuildContext context, AuthState state) {
    if (state is ShowHomeScreenState) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider.value(
                value: BlocProvider.of<AuthBloc>(context),
              ),
              BlocProvider(
                create: (context) => HotelBloc(),
              ),
            ],
            child: HomeScreen(userData: state.userData),
          ),
        ),
      );
    } else if (state is ShowLoginScreenState) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => BlocProvider.value(
            value: BlocProvider.of<AuthBloc>(context),
            child: const LoginScreen(),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: _authBlocListner,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Image.asset(
            "assets/images/em_logo.png",
          ),
        ),
      ),
    );
  }
}
