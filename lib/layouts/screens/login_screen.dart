import 'dart:io';

import 'package:em_test_app/bloc/auth_bloc/auth_bloc.dart';
import 'package:em_test_app/bloc/hotel_bloc/hotel_bloc.dart';
import 'package:em_test_app/data/services/auth_service.dart';
import 'package:em_test_app/layouts/screens/home_screen.dart';
import 'package:em_test_app/util/app_colors.dart';
import 'package:em_test_app/util/app_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
  }

  Future<void> _onPressedLoginButton() async {
    try {
      // cheking internet coneection
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        _authBloc.add(AuthLoginEvent());
      }
    } on SocketException catch (_) {
      showInSnackBar("Check your internet connection");
    }
  }

  void _authBlocListner(BuildContext context, AuthState state) {
    if (state is ShowHomeScreenState) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => MultiBlocProvider(
          providers: [
            BlocProvider.value(
              value: BlocProvider.of<AuthBloc>(context),
            ),
            BlocProvider(
              create: (context) => HotelBloc(),
            ),
          ],
          child: HomeScreen(userData: state.userData),
        ),
      ));
    } else if (state is FailureState) {
      // login failed
      showInSnackBar("Login failed, Try again");
    }
  }

  void showInSnackBar(String value) {
    var snackBar = SnackBar(content: Text(value), backgroundColor: Colors.red);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: AppColor.primary_color,
        automaticallyImplyLeading: false,
        title: Text('Login', style: AppStyle.appBarText),
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: BlocConsumer<AuthBloc, AuthState>(
            listener: _authBlocListner,
            builder: (context, state) {
              if (state is AuthLoadingState) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              return Column(
                children: [
                  SvgPicture.asset(
                    "assets/images/people.svg",
                    height: 200,
                    width: 200,
                  ),
                  const SizedBox(height: 60),
                  CupertinoButton(
                    color: AppColor.primary_color,
                    child: const Text(
                      "Login via Facebook",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _onPressedLoginButton,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
