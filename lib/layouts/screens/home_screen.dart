import 'package:em_test_app/bloc/auth_bloc/auth_bloc.dart';
import 'package:em_test_app/bloc/hotel_bloc/hotel_bloc.dart';
import 'package:em_test_app/data/model/hotel.dart';
import 'package:em_test_app/data/model/user.dart';
import 'package:em_test_app/data/services/hotel_service.dart';
import 'package:em_test_app/layouts/screens/detail_screen.dart';
import 'package:em_test_app/layouts/screens/login_screen.dart';
import 'package:em_test_app/util/app_colors.dart';
import 'package:em_test_app/util/app_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  User userData;
  HomeScreen({Key? key, required this.userData}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final HotelService _hotelService = HotelService();

  late AuthBloc _authBloc;
  late HotelBloc _hotelBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _hotelBloc = BlocProvider.of<HotelBloc>(context);
    _hotelBloc.add(LoadDataEvent());
  }

  void _onPressedLogoutButton() {
    _authBloc.add(AuthLogoutEvent());
  }

  void _authBlocListner(BuildContext context, AuthState state) {
    if (state is ShowLoginScreenState) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => BlocProvider.value(
            value: BlocProvider.of<AuthBloc>(context),
            child: const LoginScreen(),
          ),
        ),
      );
    } else if (state is FailureState) {
      // logout failed
      showInSnackBar("Something went wrong, Try again");
    }
  }

  void showInSnackBar(String value) {
    var snackBar = SnackBar(content: Text(value), backgroundColor: Colors.red);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  _onPressedHotelItem(Hotel data) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DetailScreen(hotelData: data)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: AppColor.primary_color,
        title: Text('List View', style: AppStyle.appBarText),
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: BlocListener<AuthBloc, AuthState>(
              listener: _authBlocListner,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 10),
                  Text(
                    widget.userData.name,
                    style: const TextStyle(color: Colors.black, fontSize: 28),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    widget.userData.email,
                    style: const TextStyle(color: Colors.black, fontSize: 16),
                  ),
                  const SizedBox(height: 15),
                  CupertinoButton(
                    color: AppColor.primary_color,
                    child: const Text(
                      "Logout",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _onPressedLogoutButton,
                  ),
                ],
              ),
            ),
          ),
          BlocBuilder<HotelBloc, HotelState>(
            builder: (context, state) {
              if (state is LoadingState) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (state is ShowDataState) {
                var _hotelData = state.hotelData;
                return Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: ListView.builder(
                        physics: const ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _hotelData.length,
                        itemBuilder: (BuildContext context, int i) {
                          return GestureDetector(
                            onTap: () {
                              _onPressedHotelItem(_hotelData[i]);
                            },
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(1),
                                      decoration: const BoxDecoration(
                                          color: Colors.black,
                                          shape: BoxShape.circle),
                                      child: ClipOval(
                                          child: SizedBox.fromSize(
                                        size: const Size.fromRadius(
                                            35), // Image radius
                                        child: FadeInImage.assetNetwork(
                                            // image: _hotelData[i].imageS,
                                            // image url is not working
                                            image:
                                                "https://i.natgeofe.com/n/46b07b5e-1264-42e1-ae4b-8a021226e2d0/domestic-cat_thumb_square.jpg",
                                            fit: BoxFit.cover,
                                            placeholder:
                                                'assets/images/em_logo.png'),
                                      )),
                                    ),
                                    const SizedBox(width: 10),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            _hotelData[i].title,
                                            style: const TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(height: 12),
                                          Text(
                                            _hotelData[i].address,
                                            style: const TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.grey),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 12),
                                const Divider(
                                  color: Colors.grey,
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                );
              } else if (state is LoadFailedState) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Center(
                    child: Text(
                      "Data loading failed..",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}
