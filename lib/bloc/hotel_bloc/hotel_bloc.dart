import 'package:bloc/bloc.dart';
import 'package:em_test_app/data/model/hotel.dart';
import 'package:em_test_app/data/services/hotel_service.dart';
import 'package:meta/meta.dart';

part 'hotel_event.dart';
part 'hotel_state.dart';

class HotelBloc extends Bloc<HotelEvent, HotelState> {
  final HotelService _hotelService = HotelService();

  HotelBloc() : super(LoadingState()) {
    on<LoadDataEvent>(_loadData);
  }

  Future<void> _loadData(HotelEvent event, emit) async {
    emit(LoadingState());
    List<Hotel> _allHotelData = await _hotelService.getAllHotelData();

    if (_allHotelData.isNotEmpty) {
      emit(ShowDataState(hotelData: _allHotelData));
    } else {
      emit(LoadFailedState());
    }
  }
}
