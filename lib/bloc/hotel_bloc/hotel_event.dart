part of 'hotel_bloc.dart';

@immutable
abstract class HotelEvent {}

class LoadDataEvent extends HotelEvent {}
