part of 'hotel_bloc.dart';

@immutable
abstract class HotelState {}

class LoadingState extends HotelState {}

class ShowDataState extends HotelState {
  List<Hotel> hotelData;
  ShowDataState({required this.hotelData});
}

class LoadFailedState extends HotelState {}
