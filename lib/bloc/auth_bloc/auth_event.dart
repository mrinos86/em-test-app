part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class AuthInitEvent extends AuthEvent {}

class AuthLoginEvent extends AuthEvent {}

class AuthLogoutEvent extends AuthEvent {}
