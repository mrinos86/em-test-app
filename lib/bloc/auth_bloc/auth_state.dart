part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class AuthLoadingState extends AuthState {}

class ShowHomeScreenState extends AuthState {
  User userData;
  ShowHomeScreenState({required this.userData});
}

class FailureState extends AuthState {}

class ShowLoginScreenState extends AuthState {}
