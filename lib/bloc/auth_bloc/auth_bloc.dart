import 'package:bloc/bloc.dart';
import 'package:em_test_app/data/model/user.dart';
import 'package:em_test_app/data/services/auth_service.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthService _authService = AuthService();

  AuthBloc() : super(AuthInitial()) {
    on<AuthInitEvent>(_loadAuthAnitial);
    on<AuthLogoutEvent>(_userLogout);
    on<AuthLoginEvent>(_userLogin);
  }

  void _loadAuthAnitial(AuthEvent event, emit) async {
    emit(AuthLoadingState());
    User? _userData = await _authService.checkIfIsLogged();
    if (_userData != null) {
      emit(ShowHomeScreenState(userData: _userData));
    } else {
      emit(ShowLoginScreenState());
    }
  }

  void _userLogin(AuthEvent event, emit) async {
    emit(AuthLoadingState());
    User? _userData = await _authService.login();
    if (_userData != null) {
      emit(ShowHomeScreenState(userData: _userData));
    } else {
      emit(FailureState());
    }
  }

  Future<void> _userLogout(AuthEvent event, emit) async {
    bool _result = await _authService.logOut();
    if (_result) {
      emit(ShowLoginScreenState());
    } else {
      emit(FailureState());
    }
  }
}
