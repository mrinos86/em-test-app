import 'package:em_test_app/bloc/auth_bloc/auth_bloc.dart';
import 'package:em_test_app/bloc/hotel_bloc/hotel_bloc.dart';
import 'package:em_test_app/layouts/screens/login_screen.dart';
import 'package:em_test_app/layouts/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Em Test App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => AuthBloc(),
            ),
            BlocProvider(
              create: (context) => HotelBloc(),
            ),
          ],
          child: const SplashScreen(),
        ));
  }
}
